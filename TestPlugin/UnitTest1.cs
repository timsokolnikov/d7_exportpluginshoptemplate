﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data;
using Plugin;
using DatacolPluginTemplate;
using D7_ExportPluginShopTemplate;
namespace TestPlugin
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            HandlerClass hc = new HandlerClass();
            
            List<string> columnnames = new List<string>() { "название", "цена", "описание", "изображение", "URL" };
            DataTable dt = new DataTable();
            foreach (var item in columnnames)
            {
                dt.Columns.Add(item);
            }
            dt.Rows.Add("iPhone", "11995 руб.", "Телефон создан на базе карманного видеоплеера iPod. Отличительные особенности нового телефона – сенсорная система управления, включая набор телефонного номера, на экране и наличие встроенного интернет-браузера Safari.", "iphone13z.jpg", "http://demo-ru.webasyst.net/shop/product/iphone/");

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            string error = string.Empty;
            parameters.Add("campaignname", "Test");
            parameters.Add("type", "export_plugin");
            parameters.Add("datatable", dt);
            parameters.Add("columnnames", columnnames);

            ShopPluginStaticSetting visualStaticSetting = new ShopPluginStaticSetting();

            hc.pluginHandler(parameters, out error);

            Assert.AreEqual("", error);
        }
    }
}
