set arg1=%1
set arg2=%2
set arg3=%3
set arg4=%4

copy %arg1%\*.dll %arg2%\*.dll

find /i "%arg4%" %arg3%>nul
if %errorlevel% equ 0 (
  echo "File name %arg4% found in %arg3%"
  goto ex
) else (
  echo %arg4%>>%arg3%
  echo "File name %arg4% added to %arg3%"
)

:ex
exit 0
