﻿using D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder;
using D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.GetItemInfoStrategyFolder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder
{
    public abstract class ShopPublisherBase
    {
        IShopGetItemInfoStrategy GetItemInfoStrategy;

        public ShopPublisherBase(IShopGetItemInfoStrategy _GetItemInfoStrategy)
        {
            GetItemInfoStrategy = _GetItemInfoStrategy;
        }

        public virtual void Publish(DataTable data)
        {
            //преобразовываем DataTable (таблицу данных для экспорта, которые передал Datacol) в список объектов типа ItemInfo
            //каждый объект соответственно содержит информацию об одном товаре
            List<ShopItemInfo> items = GetItemInfoStrategy.GetItems(data);

            foreach (ShopItemInfo item in items)
            {
                if (ItemExists(item))
                    UpdateItem(item);
                else
                    AddItem(item);
            }
        }

        protected abstract void AddItem(ShopItemInfo item);

        protected abstract void UpdateItem(ShopItemInfo item);

        protected abstract bool ItemExists(ShopItemInfo item);
    }
}
