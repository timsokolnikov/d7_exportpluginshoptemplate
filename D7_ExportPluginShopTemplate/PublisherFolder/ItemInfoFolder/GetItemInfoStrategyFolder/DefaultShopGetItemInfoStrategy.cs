﻿using D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.CategoryFolder.CategoryExtractionStrategyFolder;
using DatacolPluginTemplate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.GetItemInfoStrategyFolder
{
    public class DefaultShopGetItemInfoStrategy : IShopGetItemInfoStrategy
    {
        ShopPluginStaticSetting ShopPluginStaticSetting;
        ICategoryExtractionStrategy CategoryExtractionStrategy;

        public DefaultShopGetItemInfoStrategy(ShopPluginStaticSetting shopPluginStaticSetting, ICategoryExtractionStrategy categoryExtractionStrategy)
        {
            ShopPluginStaticSetting = shopPluginStaticSetting;
            CategoryExtractionStrategy = categoryExtractionStrategy;
        }

        public List<ShopItemInfo> GetItems(DataTable data)
        {
            List<ShopItemInfo> retVal = new List<ShopItemInfo>();

            DataTable dt = data.Copy();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.ToLower();
            }

            foreach (DataRow row in dt.Rows)
            {
                ShopItemInfo item = new ShopItemInfo();

                foreach (string fieldAlias in ShopPluginStaticSetting.ItemnameColumnAliases)
                {
                    item.Name = ExtractCellValue(row, fieldAlias.ToLower());
                    if (!String.IsNullOrEmpty(item.Name)) break;
                }

                if (String.IsNullOrEmpty(item.Name)) throw new Exception("Пустое имя товара");

                foreach (string fieldAlias in ShopPluginStaticSetting.DescriptionColumnAliases)
                {
                    item.Description = ExtractCellValue(row, fieldAlias.ToLower());
                    if (!String.IsNullOrEmpty(item.Description)) break;
                }

                foreach (string fieldAlias in ShopPluginStaticSetting.CategoryColumnAliases)
                {
                    string categoryRawString = ExtractCellValue(row, fieldAlias.ToLower());

                    if (!String.IsNullOrEmpty(categoryRawString))
                    {
                        if (CategoryExtractionStrategy != null)
                        {
                            item.Category = CategoryExtractionStrategy.Extract(categoryRawString);
                        }
                        else
                        {
                            item.Category = new CategoryFolder.Category(categoryRawString);
                        }

                        break;
                    }
                }

                foreach (string fieldAlias in ShopPluginStaticSetting.PhotoColumnAliases)
                {
                    string photoRawString = ExtractCellValue(row, fieldAlias.ToLower());

                    if (!String.IsNullOrEmpty(photoRawString))
                    {
                        item.PhotoList = photoRawString.Split(',').ToList();
                        break;
                    }
                }

                foreach (string fieldAlias in ShopPluginStaticSetting.PriceColumnAliases)
                {
                    string priceRawString = ExtractCellValue(row, fieldAlias.ToLower());

                    if (!String.IsNullOrEmpty(priceRawString))
                    {
                        priceRawString = priceRawString.Replace(".", ",");
                        priceRawString = Regex.Replace(priceRawString, "[^\\d]{1,}$", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
                        item.Price = Convert.ToDouble(priceRawString);
                        break;
                    }
                }

                retVal.Add(item);
            }

            return retVal;
        }

        public static string ExtractCellValue(DataRow dr, string columnName)
        {
            string retVal = "";

            try
            {
                retVal = dr[columnName].ToString();
            }
            catch
            {

            }

            return retVal;
        }

        //public static double ExtractCellValue(DataRow dr, string columnName)
        //{
        //    double retVal = "";

        //    string strValue = 

        //    try
        //    {
        //        retVal = dr[columnName].ToString();
        //    }
        //    catch
        //    {

        //    }

        //    return retVal;
        //}
    }
}
