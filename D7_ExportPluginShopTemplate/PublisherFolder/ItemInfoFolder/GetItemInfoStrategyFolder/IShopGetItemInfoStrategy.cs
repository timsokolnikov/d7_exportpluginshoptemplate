﻿using System.Collections.Generic;
using System.Data;
namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.GetItemInfoStrategyFolder
{
    public interface IShopGetItemInfoStrategy
    {
        List<ShopItemInfo> GetItems(DataTable data);
    }
}