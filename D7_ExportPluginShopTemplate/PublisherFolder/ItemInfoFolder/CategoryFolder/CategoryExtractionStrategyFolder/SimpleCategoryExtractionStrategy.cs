﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.CategoryFolder.CategoryExtractionStrategyFolder
{
    class SimpleCategoryExtractionStrategy : ICategoryExtractionStrategy
    {
        public Category Extract(object category)
        {
            if (category==null || String.IsNullOrEmpty(category.ToString())) return null;

            return new Category(category.ToString());
        }
    }
}
