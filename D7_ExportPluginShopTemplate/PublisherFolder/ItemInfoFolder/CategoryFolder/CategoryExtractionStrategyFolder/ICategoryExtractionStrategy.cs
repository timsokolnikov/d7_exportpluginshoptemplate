﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.CategoryFolder.CategoryExtractionStrategyFolder
{
    public interface ICategoryExtractionStrategy
    {
        Category Extract(object category);
    }
}
