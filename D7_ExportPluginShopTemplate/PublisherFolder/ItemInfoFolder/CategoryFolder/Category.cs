﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.CategoryFolder
{
    public class Category
    {
        public string UID;
        public string Name;
        public Category ParentCategory;

        public Category()
        {
            UID = "";
            Name = "";
            ParentCategory = null;
        }
        public Category(string _UID, string _Name)
        {
            UID = _UID;
            Name = _Name;
            ParentCategory = null;
        }

        public Category(string _UID, string _Name, Category _ParentCategory)
        {
            UID = _UID;
            Name = _Name;
            ParentCategory = _ParentCategory;
        }

        public Category(string _Name)
        {
            UID = "";
            Name = _Name;
            ParentCategory = null;
        }
        public static List<string> GetCategoryPath(Category category)
        {
            List<string> retVal = new List<string>();

            if (category.ParentCategory != null)
            {
                retVal = GetCategoryPath(category.ParentCategory);
                retVal.Add(category.Name);
            }
            else
            {
                retVal.Add(category.Name);
            }
            return retVal;
        }

        public virtual bool Compare(Category category)
        {
            if (category == null) return false;

            if (category.UID != UID) return false;

            if (category.ParentCategory == null) return true;

            return category.ParentCategory.Compare(category.ParentCategory);
        }
    }
}
