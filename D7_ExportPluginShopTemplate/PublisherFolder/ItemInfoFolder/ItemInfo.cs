﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder
{
    public class ItemInfo
    {
        private string mId;

        public string Id
        {
            get { return mId; }
            set { mId = value; }
        }

        private string mName;

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public ItemInfo(string name)
        {
            mName = name;
            mId = name;
        }

        public ItemInfo()
        {
            mId = "";
            mName = "";
        }
    }
}
