﻿using D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder.CategoryFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D7_ExportPluginShopTemplate.PublisherFolder.ItemInfoFolder
{
    public class ShopItemInfo : ItemInfo
    {
        private double mPrice;

        public double Price
        {
            get { return mPrice; }
            set { mPrice = value; }
        }

        private string mDescription;

        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        private Category mCategory;

        public Category Category
        {
            get { return mCategory; }
            set { mCategory = value; }
        }

        private List<string> mPhotoList;

        public List<string> PhotoList
        {
            get { return mPhotoList; }
            set { mPhotoList = value; }
        }

        public ShopItemInfo(string name, double mPrice, string mDescription, Category mCategory, List<string> mPhotoList) : base(name)
        {
            this.mPrice = mPrice;
            this.mDescription = mDescription;
            this.mCategory = mCategory;
            this.mPhotoList = mPhotoList;
        }

        public ShopItemInfo():base()
        {
            this.mPrice = 0;
            this.mDescription = "";
            this.mCategory = null;
            this.mPhotoList = new List<string>();
        }
    }
}
