﻿using PluginInterface;
using PluginInterface.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using UIs;

namespace DatacolPluginTemplate
{
    public class ShopPluginStaticSetting : PluginStaticSetting
    {

        public ShopPluginStaticSetting()
            : base()
        {
            mItemnameColumnAliases = new List<string> { "наименование", "название", "name" };
            mDescriptionColumnAliases = new List<string> { "описание", "description" };
            mCategoryColumnAliases = new List<string> { "категория", "category","рубрика" };
            mPhotoColumnAliases = new List<string> { "фото", "изображение","photo" };
            mPriceColumnAliases = new List<string> { "цена", "price","стоимость" };
        }

        private List<string> mItemnameColumnAliases;
        [Category("Базовые")]
        [DisplayNameAttribute("Поле с наименованием")]
        [DescriptionAttribute("Варианты названия поля данных, в которое собирается наименование")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> ItemnameColumnAliases
        {
            get { return mItemnameColumnAliases; }
            set { mItemnameColumnAliases = value; }
        }

        private List<string> mDescriptionColumnAliases;
        [Category("Базовые")]
        [DisplayNameAttribute("Поле с описанием")]
        [DescriptionAttribute("Варианты названия поля данных, в которое собирается описание")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> DescriptionColumnAliases
        {
            get { return mDescriptionColumnAliases; }
            set { mDescriptionColumnAliases = value; }
        }

        private List<string> mCategoryColumnAliases;
        [Category("Базовые")]
        [DisplayNameAttribute("Поле с категорией")]
        [DescriptionAttribute("Варианты названия поля данных, в которое собирается категория")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> CategoryColumnAliases
        {
            get { return mCategoryColumnAliases; }
            set { mCategoryColumnAliases = value; }
        }

        private List<string> mPhotoColumnAliases;
        [Category("Базовые")]
        [DisplayNameAttribute("Поле с фотографиями")]
        [DescriptionAttribute("Варианты названия поля данных, в которое собираются названия фотографий")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> PhotoColumnAliases
        {
            get { return mPhotoColumnAliases; }
            set { mPhotoColumnAliases = value; }
        }

        private List<string> mPriceColumnAliases;
        [Category("Базовые")]
        [DisplayNameAttribute("Поле с ценой")]
        [DescriptionAttribute("Варианты названия поля данных, в которое собирается цена")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> PriceColumnAliases
        {
            get { return mPriceColumnAliases; }
            set { mPriceColumnAliases = value; }
        }
    }
}
